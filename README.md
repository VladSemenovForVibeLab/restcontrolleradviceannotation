# Пример приложения на Spring Boot с использованием Swagger, Docker и Docker Compose

Пример простого веб-приложения на Spring Boot, которое демонстрирует создание глобальных исключений. Приложение позволяет создавать и получать продукты, а также создавать и получать людей.

## Требования

- Java 17
- Docker
- Docker Compose

## Запуск приложения

1. Склонируйте репозиторий:

git clone https://gitlab.com/VladSemenovForVibeLab/restcontrolleradviceannotation.git

2. Перейдите в каталог проекта:

bash
cd RestControllerAdviceAnnotation

3. Соберите приложение:

mvn clean package -DskipTests

4. Запустите приложение:

docker-compose build
docker-compose up

5. После успешного запуска, приложение будет доступно по следующему URL:

    - Swagger UI: http://localhost:8080/swagger-ui.html

## Использование API

- Создание продукта:

bash
POST /insertProduct
Content-Type: application/json

{
"id": 1,
"name": "Mobile",
"price": 20000.0,
"qty": 1
}

- Получение списка продуктов:

bash
GET /products

- Получение списка людей:

bash
GET /users

