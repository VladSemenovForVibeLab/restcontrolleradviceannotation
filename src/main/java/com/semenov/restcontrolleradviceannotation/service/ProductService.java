package com.semenov.restcontrolleradviceannotation.service;

import com.semenov.restcontrolleradviceannotation.entity.Product;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    private static final List<Product> products = new ArrayList<>();

    @PostConstruct
    private  void init(){
        Product product = new Product(1,"Mobile",20000.0,1);
        products.add(product);
    }

    public List<Product> getProducts(){
        return products;
    }

    public Product insertProduct(Product product){
        if(product.getName()==null){
            throw new NullPointerException("Product name cannot be null! ");
        }
        products.add(product);
        return product;
    }

}
