package com.semenov.restcontrolleradviceannotation.service;

import com.semenov.restcontrolleradviceannotation.entity.Product;
import com.semenov.restcontrolleradviceannotation.entity.User;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
    private static final List<User> users = new ArrayList<>();

    @PostConstruct
    private  void init(){
        User user = new User(1,"Vlad","Kiev");
        users.add(user);
    }

    public List<User> getUsers(){
        return users;
    }

    public User insertUser(User user){
        if(user.getName()==null){
            throw new NullPointerException("UserName cannot be null! ");
        }
        users.add(user);
        return user;
    }

}
