package com.semenov.restcontrolleradviceannotation.controller;

import com.semenov.restcontrolleradviceannotation.entity.Product;
import com.semenov.restcontrolleradviceannotation.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {
    @Autowired
    private ProductService productService;
    @GetMapping("/products")
    public List<Product> getAllProducts(){
       return productService.getProducts();
    }

    @PostMapping("/insertProduct")
    public Product insertProduct(@RequestBody Product product) throws NullPointerException{
        return productService.insertProduct(product);
    }

}
