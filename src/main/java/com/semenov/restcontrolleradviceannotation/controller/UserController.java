package com.semenov.restcontrolleradviceannotation.controller;

import com.semenov.restcontrolleradviceannotation.entity.User;
import com.semenov.restcontrolleradviceannotation.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userService.getUsers();
    }
    @PostMapping("/insertUser")
    public User insertUser(@RequestBody User user){
        return userService.insertUser(user);
    }
}
