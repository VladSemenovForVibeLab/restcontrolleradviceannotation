package com.semenov.restcontrolleradviceannotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class RestControllerAdviceAnnotationApplication {
    public static void main(String[] args) {
        SpringApplication.run(RestControllerAdviceAnnotationApplication.class, args);
    }
}
