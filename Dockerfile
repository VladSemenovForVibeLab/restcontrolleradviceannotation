FROM openjdk:17-jdk-alpine
COPY target/RestControllerAdviceAnnotation-0.0.1-SNAPSHOT.jar backend-2.0.0.jar
ENTRYPOINT ["java","-jar","backend-2.0.0.jar"]
